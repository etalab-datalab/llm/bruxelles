#!/usr/bin/bash
set -e

ROOT=$(dirname $0)
cd $ROOT

module purge
module load python/3.10.4
#module load pytorch-gpu/py3/2.1.1

python3 -m venv .venv
.venv/bin/pip install --upgrade pip
.venv/bin/pip install --no-cache-dir -r requirements.txt

source .venv/bin/activate

python download_deps.py
