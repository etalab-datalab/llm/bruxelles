# Resource URL: https://transparency.fb.com/fr-fr/researchtools/ad-library-tools
### Ad Library

[Meta Ad Library](https://www.facebook.com/adlibrary) is a comprehensive, searchable database for ads transparency. People can use the Ad Library to get more information about the ads they see across Meta technologies.

People can search for all active ads running across products from Meta. For ads about social issues, elections or politics, Meta provides additional information, including spend, reach and funding entities. These ads are visible whether they’re active or inactive and are stored in the Ad Library for 7 years.

Meta offers more information for ads that deliver an impression in the EU or associated territories. These ads are displayed in the Ad Library while active and archived for one year upon the delivery of their last impression. The Ad Library also has a searchable database that displays all active, public branded content running on Facebook and Instagram with a paid partnership label.

[Learn more](https://www.facebook.com/business/help/2405092116183307?id=288762101909005.) about the types of data available in the Ad Library.

### Ad Library API

The [Ad Library API](https://www.facebook.com/ads/library/api/) is an application programming interface that allows for a deeper analysis of ads about social issues, elections or politics, as well as ads that deliver to the [EU and associated territories](https://www.facebook.com/business/help/605021638170961). Authorized users can also analyze active and public branded content running on Facebook and Instagram via the API.

### Ad Library Report

The [Ad Library Report](https://www.facebook.com/ads/library/report/) provides an aggregated and comprehensive view of ads about social issues, elections or politics in a selected country for a given time period. The Ad Library Report, which can be downloaded, is available in [all countries](https://www.facebook.com/business/help/2150157295276323) that require authorization to run ads about social issues, elections or politics.

### Ad Targeting dataset

The [Ad Targeting](https://developers.facebook.com/docs/fort-ads-targeting-dataset) dataset allows approved researchers to analyze targeting information selected by advertisers who ran ads about social issues, elections or politics any time after August 2020 in more than 120 countries. All active and inactive social issues, as well as electoral and political ads with at least one impression are included and remain in the dataset for 7 years.