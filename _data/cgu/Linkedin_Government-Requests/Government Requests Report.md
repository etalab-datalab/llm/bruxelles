# Resource URL: https://about.linkedin.com/transparency/government-requests-report
Government Requests Report
==========================

  
This report discusses how we respond to government requests for member data and removal of content. As part of our commitment to a safe, trusted, and professional environment, we provide transparency about government requests for member data and content removal we receive. We believe in free expression and try to minimize the impact of laws that negatively affect constructive, relevant speech.

We respect the laws that apply to us in the countries where we operate. In the U.S., we respond to several types of legal process at the federal, state, and local levels. Most often, we’re responding to search warrants and subpoenas. Outside the U.S., we ask the government that’s requesting information to issue its request properly, for example through a Mutual Legal Assistance Treaty or a form of international process known as a letter rogatory. Sometimes, we’ll make an exception in an emergency. For more information, see our [Law Enforcement Data Request Guidelines](https://www.linkedin.com/help/linkedin/answer/16880/linkedin-law-enforcement-data-request-guidelines?lang=en).  
  
We never provide information to a government to fulfill a member data request without first trying to notify the individual member, except when applicable law prohibits us from doing so. We may make exceptions in certain emergencies.  
  
This report covers the six-month period between January 1 and June 30, 2023.  

This report covers:
-------------------

[Global requests for data](#government-requests-data)

[U.S. government requests for data](#us-government-requests)

[Global content removal requests](#content-removal-requests)

Government requests for data: global
------------------------------------

### Requests in the U.S. and globally

### Requests in the U.S. and globally

### Requests in the U.S. and globally

### Requests in the U.S. and globally

### Requests in the U.S. and globally

Requests by country
-------------------

### Requests by country and accounts subject to those requests

#### 2023: January-June

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Argentina | 2   | 2   | 0%  | 0   |
| Australia | 2   | 3   | 50% | 1   |
| Austria | 6   | 7   | 0%  | 0   |
| Bangladesh | 1   | 1   | 0%  | 0   |
| Belgium | 4   | 5   | 100% | 4   |
| Bolivia | 1   | 2   | 0%  | 0   |
| Brazil | 18  | 20  | 44% | 8   |
| Canada | 8   | 10  | 75% | 8   |
| Chile | 1   | 1   | 0%  | 0   |
| China | 3   | 0   | 0%  | 0   |
| Columbia | 2   | 2   | 0%  | 0   |
| Czech Republic | 1   | 1   | 100% | 1   |
| Estonia | 2   | 2   | 0%  | 0   |
| Finland | 1   | 2   | 0%  | 0   |
| France | 63  | 68  | 57% | 39  |
| Germany | 65  | 102 | 82% | 64  |
| Greece | 3   | 3   | 33% | 1   |
| Hungary | 1   | 1   | 100% | 1   |
| India | 81  | 116 | 21% | 19  |
| Ireland | 12  | 12  | 58% | 7   |
| Italy | 103 | 250 | 43% | 88  |
| Malta | 1   | 1   | 0%  | 0   |
| Mexico | 7   | 8   | 0%  | 0   |
| Netherlands | 21  | 54  | 95% | 52  |
| Poland | 8   | 21  | 38% | 3   |
| Portugal | 2   | 2   | 50% | 1   |
| Romania | 1   | 4   | 0%  | 0   |
| Singapore | 2   | 2   | 0%  | 0   |
| Spain | 16  | 17  | 44% | 7   |
| Switzerland | 2   | 2   | 0%  | 0   |
| Taiwan | 1   | 1   | 0%  | 0   |
| United Kingdom | 17  | 22  | 53% | 10  |
| United States | 386 | 2053 | 83% | 1306 |
| Venezuela | 1   | 1   | 0%  | 0   |
| **Total** | **845** | **2798** | **64%** | **1620** |

### Requests by country and accounts subject to those requests

#### 2022: July-December

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Argentina | 1   | 23  | 0%  | 0   |
| Austria | 1   | 1   | 100% | 1   |
| Belgium | 2   | 2   | 100% | 2   |
| Brazil | 22  | 34  | 59% | 13  |
| Canada | 3   | 3   | 33% | 1   |
| China | 2   | 4   | 50% | 1   |
| Czech Republic | 1   | 1   | 100% | 1   |
| France | 21  | 23  | 71% | 15  |
| Germany | 54  | 63  | 78% | 48  |
| India | 65  | 134 | 26% | 21  |
| Ireland | 1   | 1   | 0%  | 0   |
| Italy | 34  | 34  | 44% | 15  |
| Jordan | 2   | 2   | 0%  | 0   |
| Netherlands | 5   | 50  | 100% | 48  |
| Pakistan | 1   | 1   | 0%  | 0   |
| Poland | 1   | 1   | 0%  | 0   |
| Portugal | 1   | 3   | 0%  | 0   |
| Romania | 1   | 1   | 0%  | 0   |
| Spain | 10  | 15  | 70% | 12  |
| Switzerland | 2   | 3   | 0%  | 0   |
| Turkey | 1   | 1   | 100% | 1   |
| United Kingdom | 27  | 45  | 44% | 17  |
| United States | 391 | 2636 | 80% | 949 |
| Venezuela | 1   | 1   | 0%  | 0   |
| **Total** | **649** | **3081** | **69%** | **1144** |

### Requests by country and accounts subject to those requests

#### 2022: January-June

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Argentina | 3   | 3   | 0%  | 0   |
| Australia | 1   | 1   | 0%  | 0   |
| Austria | 2   | 2   | 0%  | 0   |
| Belgium | 6   | 13  | 100% | 10  |
| Brazil | 12  | 35  | 50% | 7   |
| Canada | 5   | 7   | 80% | 6   |
| China | 6   | 6   | 33% | 2   |
| Costa Rica | 1   | 5   | 0%  | 0   |
| Denmark | 2   | 3   | 100% | 2   |
| Finland | 1   | 1   | 0%  | 0   |
| France | 22  | 35  | 73% | 24  |
| Georgia | 1   | 1   | 0%  | 0   |
| Germany | 68  | 81  | 75% | 52  |
| Hungary | 2   | 3   | 50% | 1   |
| India | 64  | 84  | 8%  | 5   |
| Ireland | 5   | 5   | 20% | 1   |
| Italy | 11  | 23  | 73% | 14  |
| Jordan | 1   | 2   | 0%  | 0   |
| Malta | 3   | 3   | 0%  | 0   |
| Mexico | 2   | 2   | 0%  | 0   |
| Monaco | 1   | 2   | 100% | 1   |
| Nepal | 1   | 1   | 0%  | 0   |
| Netherlands | 10  | 75  | 40% | 9   |
| New Zealand | 1   | 1   | 0%  | 0   |
| Peru | 1   | 1   | 0%  | 0   |
| Poland | 3   | 3   | 33% | 1   |
| Portugal | 2   | 4   | 50% | 1   |
| Romania | 1   | 1   | 0%  | 0   |
| Singapore | 1   | 1   | 0%  | 0   |
| Spain | 11  | 12  | 36% | 5   |
| Switzerland | 5   | 5   | 20% | 1   |
| United Arab Emirates | 1   | 1   | 0%  | 0   |
| United Kingdom | 21  | 24  | 43% | 11  |
| United States | 327 | 1114 | 81% | 572 |
| **Total** | **604** | **1560** | **64%** | **725** |

### Requests by country and accounts subject to those requests

#### 2021: July-December

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Argentina | 4   | 4   | 0%  | 0   |
| Australia | 1   | 1   | 0%  | 0   |
| Belgium | 8   | 9   | 50% | 5   |
| Brazil\* | 3   | 6   | 100% | 6   |
| Canada | 1   | 1   | 0%  | 0   |
| China | 4   | 4   | 50% | 2   |
| Czech Republic | 1   | 1   | 100% | 1   |
| France | 37  | 43  | 65% | 27  |
| Germany | 79  | 92  | 77% | 69  |
| Greece | 3   | 3   | 33% | 1   |
| Hungary | 1   | 1   | 0%  | 0   |
| India | 46  | 49  | 0%  | 0   |
| Ireland | 1   | 1   | 0%  | 0   |
| Italy | 8   | 9   | 0%  | 0   |
| Malta | 1   | 1   | 0%  | 0   |
| Mexico | 1   | 1   | 0%  | 0   |
| Netherlands | 11  | 23  | 82% | 12  |
| New Zealand | 1   | 1   | 100% | 1   |
| Poland | 5   | 5   | 20% | 1   |
| Singapore | 1   | 1   | 0%  | 0   |
| Spain | 12  | 12  | 33% | 4   |
| Switzerland | 6   | 6   | 67% | 4   |
| United Kingdom | 15  | 20  | 47% | 8   |
| United States | 302 | 1190 | 83% | 676 |
| **Total** | **552** | **1484** | **67%** | **817** |

_\*An earlier version of this report inadvertently omitted the data shown in the table above for Brazilian requests made to LinkedIn during the reporting period of July through December 2021._

### Requests by country and accounts subject to those requests

#### 2021: January-June

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Argentina | 1   | 1   | 0%  | 0   |
| Belgium | 4   | 4   | 0%  | 4   |
| Brazil | 3   | 6   | 0%  | 0   |
| Canada | 4   | 4   | 100% | 4   |
| Chile | 2   | 2   | 0%  | 0   |
| China | 5   | 5   | 80% | 4   |
| Colombia | 1   | 1   | 0%  | 0   |
| France | 56  | 74  | 73% | 47  |
| Germany | 62  | 70  | 77% | 52  |
| Greece | 1   | 1   | 100% | 1   |
| Hungary | 1   | 1   | 100% | 1   |
| India | 30  | 37  | 0%  | 0   |
| Ireland | 6   | 6   | 33% | 2   |
| Italy | 23  | 36  | 61% | 17  |
| Mexico | 2   | 2   | 0%  | 0   |
| Monaco | 1   | 1   | 100% | 1   |
| Netherlands | 21  | 28  | 76% | 18  |
| Nigeria | 1   | 1   | 0%  | 0   |
| Pakistan | 1   | 1   | 0%  | 0   |
| Peru | 3   | 3   | 100% | 3   |
| Poland | 3   | 3   | 0%  | 0   |
| Portugal | 4   | 4   | 0%  | 0   |
| Singapore | 1   | 1   | 0%  | 0   |
| Slovenia | 2   | 2   | 0%  | 0   |
| South Korea | 1   | 1   | 0%  | 0   |
| Spain | 9   | 10  | 33% | 3   |
| Sweden | 1   | 1   | 100% | 1   |
| Switzerland | 2   | 2   | 50% | 1   |
| United Kingdom | 12  | 15  | 58% | 9   |
| United States | 362 | 2349 | 83% | 901 |
| **Total** | **625** | **2672** | **72%** | **1069** |

### Requests by country and accounts subject to those requests

#### 2020: July-December

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Argentina | 4   | 4   | 0%  | 0   |
| Australia | 4   | 5   | 50% | 2   |
| Belgium | 2   | 4   | 100% | 4   |
| Brazil | 4   | 4   | 50% | 2   |
| Canada | 1   | 1   | 100% | 1   |
| China | 2   | 2   | 0%  | 0   |
| Columbia | 2   | 2   | 0%  | 0   |
| Czech Republic | 1   | 1   | 0%  | 0   |
| Dominican Republic | 1   | 1   | 0%  | 0   |
| Estonia | 2   | 2   | 50% | 1   |
| France | 26  | 33  | 46% | 14  |
| Georgia | 1   | 1   | 0%  | 0   |
| Germany | 50  | 82  | 50% | 38  |
| India | 29  | 32  | 0%  | 0   |
| Italy | 7   | 8   | 57% | 5   |
| Monaco | 1   | 1   | 100% | 1   |
| Netherlands | 16  | 33  | 69% | 19  |
| Norway | 1   | 1   | 100% | 1   |
| Pakistan | 1   | 1   | 0%  | 0   |
| Peru | 1   | 1   | 0%  | 0   |
| Poland | 2   | 2   | 0%  | 0   |
| Portugal | 2   | 2   | 0%  | 0   |
| Serbia | 2   | 3   | 0%  | 0   |
| Singapore | 3   | 3   | 0%  | 0   |
| Slovenia | 2   | 8   | 50% | 2   |
| Spain | 8   | 9   | 38% | 3   |
| Sweden | 1   | 1   | 100% | 1   |
| Switzerland | 3   | 3   | 67% | 2   |
| United Kingdom | 5   | 5   | 40% | 1   |
| United States | 364 | 1,420 | 76% | 702 |
| **Total** | **548** | **1,675** | **63%** | **799** |

### Requests by country and accounts subject to those requests

#### 2020: January-June

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Belgium | 1   | 1   | 0%  | 0   |
| Brazil | 5   | 5   | 80% | 4   |
| Canada | 1   | 1   | 100% | 1   |
| Columbia | 1   | 1   | 0%  | 0   |
| Finland | 1   | 1   | 0%  | 0   |
| France | 13  | 16  | 62% | 10  |
| Germany | 62  | 92  | 74% | 63  |
| Greece | 2   | 2   | 0%  | 0   |
| India | 11  | 14  | 0%  | 0   |
| Italy | 3   | 4   | 100% | 4   |
| Malta | 2   | 2   | 0%  | 0   |
| Netherlands | 7   | 11  | 86% | 9   |
| Poland | 1   | 7   | 0%  | 0   |
| Portugal | 1   | 1   | 0%  | 0   |
| Serbia | 1   | 1   | 0%  | 0   |
| Singapore | 3   | 5   | 33% | 2   |
| Slovenia | 1   | 2   | 0%  | 0   |
| Spain | 4   | 4   | 25% | 1   |
| Switzerland | 2   | 2   | 0%  | 0   |
| United Kingdom | 10  | 11  | 30% | 3   |
| United States | 377 | 2,459 | 78% | 1,862 |
| **Total** | **509** | **2,642** | **72%** | **1,959** |

### Requests by country and accounts subject to those requests

#### 2019: July-December

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Brazil | 11  | 11  | 82% | 9   |
| Canada | 1   | 1   | 0%  | 0   |
| China | 1   | 1   | 0%  | 0   |
| France | 8   | 8   | 88% | 7   |
| Germany | 33  | 81  | 85% | 71  |
| India | 9   | 11  | 0%  | 0   |
| Ireland | 1   | 4   | 0%  | 0   |
| Malta | 1   | 1   | 0%  | 0   |
| Monaco | 1   | 1   | 100% | 1   |
| Netherlands | 5   | 5   | 80% | 4   |
| Spain | 6   | 7   | 17% | 1   |
| UK  | 4   | 4   | 0%  | 0   |
| US  | 349 | 1,488 | 82% | 819 |
| **Total** | **430** | **1,623** | **78%** | **912** |

### Requests by country and accounts subject to those requests

#### 2019: January-June

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Brazil | 8   | 12  | 88% | 9   |
| Canada | 2   | 2   | 100% | 2   |
| China | 1   | 1   | 0%  | 0   |
| Dominican Republic | 1   | 1   | 0%  | 0   |
| France | 6   | 9   | 67% | 4   |
| Germany | 9   | 14  | 44% | 4   |
| India | 1   | 10  | 0%  | 0   |
| Italy | 1   | 1   | 0%  | 0   |
| Netherlands | 2   | 2   | 50% | 1   |
| Poland | 1   | 1   | 100% | 1   |
| Portugal | 2   | 2   | 0%  | 0   |
| Spain | 1   | 1   | 0%  | 0   |
| Switzerland | 2   | 2   | 50% | 1   |
| UK  | 3   | 5   | 0%  | 0   |
| US  | 314 | 1,170 | 82% | 639 |
| **Total** | **362** | **1,233** | **77%** | **662** |

### Requests by country and accounts subject to those requests

#### 2018: July-December

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Brazil | 10  | 13  | 31% | 4   |
| Canada | 3   | 3   | 67% | 2   |
| China | 2   | 2   | 100% | 2   |
| France | 11  | 14  | 29% | 4   |
| Germany | 3   | 4   | 75% | 3   |
| India | 7   | 9   | 0%  | 0   |
| Ireland | 1   | 1   | 100% | 1   |
| Italy | 2   | 2   | 0%  | 0   |
| Spain | 4   | 4   | 0%  | 0   |
| United Kingdom | 1   | 1   | 0%  | 0   |
| United States | 203 | 1,069 | 52% | 553 |
| **Total** | **247** | **1,122** | **41%** | **569** |

### Requests by country and accounts subject to those requests

#### 2018: January-June

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Australia | 2   | 2   | 0%  | 0   |
| Brazil | 6   | 8   | 63% | 5   |
| Canada | 3   | 3   | 0%  | 0   |
| France | 2   | 10  | 10% | 1   |
| Germany | 12  | 15  | 73% | 11  |
| India | 5   | 5   | 0%  | 0   |
| Italy | 1   | 1   | 100% | 1   |
| Qatar | 1   | 1   | 0%  | 0   |
| Singapore | 2   | 2   | 0%  | 0   |
| United Kingdom | 2   | 2   | 100% | 2   |
| United States | 196 | 1,077 | 37% | 399 |
| **Total** | **232** | **1,126** | **35%** | **419** |

### Requests by country and accounts subject to those requests

#### 2017: July-December

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Brazil | 5   | 8   | 55% | 1   |
| Canada | 1   | 1   | 100% | 1   |
| Germany | 2   | 2   | 50% | 1   |
| India | 9   | 9   | 0%  | 0   |
| Italy | 9   | 9   | 20% | 5   |
| Netherlands | 1   | 1   | 100% | 1   |
| Puerto Rico | 1   | 1   | 0%  | 0   |
| Spain | 1   | 1   | 100% | 1   |
| Sweden | 1   | 1   | 0%  | 0   |
| Switzerland | 1   | 4   | 100% | 3   |
| United Kingdom | 2   | 3   | 0%  | 0   |
| United States | 191 | 602 | 76% | 331 |
| **Total** | **224** | **642** | **63%** | **344** |

### Requests by country and accounts subject to those requests

#### 2017: January-June

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Brazil | 3   | 3   | 33% | 1   |
| China | 1   | 1   | 100% | 1   |
| Czech Republic | 1   | 1   | 0%  | 0   |
| France | 1   | 1   | 0%  | 0   |
| Germany | 2   | 2   | 50% | 1   |
| India | 3   | 3   | 0%  | 0   |
| Ireland | 1   | 1   | 100% | 1   |
| Italy | 1   | 1   | 100% | 1   |
| Netherlands | 1   | 1   | 0%  | 0   |
| Poland | 1   | 1   | 100% | 1   |
| Spain | 3   | 3   | 67% | 2   |
| United Kingdom | 2   | 2   | 0%  | 0   |
| United States | 187 | 594 | 71% | 229 |
| **Total** | **207** | **614** | **68%** | **237** |

### Government requests for data

#### 2016: July-December

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Brazil | 4   | 5   | 75% | 3   |
| China | 1   | 13  | 100% | 13  |
| France | 1   | 1   | 0%  | 0   |
| Greece | 1   | 1   | 100% | 1   |
| India | 3   | 3   | 0%  | 0   |
| Italy | 1   | 1   | 100% | 1   |
| Macedonia | 1   | 1   | 0%  | 0   |
| Philippines | 1   | 1   | 100% | 1   |
| Spain | 1   | 1   | 0%  | 0   |
| Sweden | 1   | 1   | 0%  | 0   |
| United States | 135 | 345 | 74% | 192 |
| **Total** | **150** | **373** | **73%** | **211** |

### Government requests for data

#### 2016: January-June

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts for which LinkedIn provided some data \[2\] |
| Brazil | 3   | 4   | 33% | 1   |
| Canada | 1   | 1   | 100% | 1   |
| France | 2   | 2   | 50% | 1   |
| Germany | 2   | 2   | 0%  | 0   |
| India | 6   | 7   | 0%  | 0   |
| Poland | 1   | 1   | 0%  | 0   |
| Spain | 1   | 1   | 0%  | 0   |
| Turkey | 1   | 1   | 0%  | 0   |
| United States | 128 | 291 | 68% | 150 |
| **Total** | **145** | **310** | **62%** | **153** |

### Government requests for data

#### 2015: July-December

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts impacted (LinkedIn provided some data) \[2\] |
| Brazil | 1   | 1   | 0%  | 0   |
| China | 1   | 1   | 0%  | 0   |
| France | 1   | 1   | 0%  | 0   |
| Germany | 2   | 2   | 0%  | 0   |
| India | 5   | 5   | 0%  | 0   |
| Liechtenstein | 1   | 1   | 0%  | 0   |
| United Kingdom | 1   | 1   | 0%  | 0   |
| United States | 127 | 226 | 72% | 134 |
| **Total** | **139** | **238** | **66%** | **134** |

### Government requests for data

#### 2015: January-June

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts impacted (LinkedIn provided some data) \[2\] |
| India | 4   | 4   | 0%  | 0   |
| Bulgaria | 2   | 2   | 50% | 1   |
| Canada | 2   | 2   | 100% | 2   |
| France | 2   | 2   | 0%  | 0   |
| Spain | 1   | 8   | 0%  | 0   |
| United Kingdom | 2   | 2   | 0%  | 0   |
| United States | 99  | 141 | 78% | 113 |
| **Total** | **112** | **161** | **74%** | **116** |

### Government requests for data

#### 2014: July-December

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts impacted (LinkedIn provided some data) \[2\] |
| Brazil | 3   | 3   | 33% | 1   |
| France | 3   | 3   | 0%  | 0   |
| Germany | 3   | 3   | 0%  | 0   |
| India | 5   | 5   | 0%  | 0   |
| Ireland | 1   | 1   | 100% | 1   |
| Singapore | 1   | 1   | 0%  | 0   |
| United States | 84  | 202 | 70% | 76  |
| **Total** | **100** | **218** | **60%** | **78** |

### Government requests for data

#### 2014: January-June

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
|     | Requests for member data | Accounts subject to request(s) \[1\] | Percentage of requests for which LinkedIn provided some data | Accounts impacted (LinkedIn provided some data) \[2\] |
| Argentina | 2   | 2   | 0%  | 0   |
| Australia | 2   | 2   | 50% | 1   |
| Brazil | 1   | 1   | 100% | 1   |
| Canada | 4   | 51  | 35% | 45  |
| France | 4   | 4   | 0%  | 0   |
| Germany | 1   | 1   | 0%  | 0   |
| Hong Kong | 1   | 1   | 0%  | 0   |
| India | 4   | 4   | 25% | 1   |
| Spain | 1   | 8   | 0%  | 0   |
| United Kingdom | 1   | 1   | 100% | 1   |
| United States | 85  | 1,069 | 65% | 101 |
| **Total** | **116** | **1,144** | **52%** | **150** |

\[1\] This column was previously labeled "Accounts Impacted", but we changed the name to clarify that it reflects the number of accounts subject to the data requests, and not the number of accounts for which some responsive data was in fact provided.

  
\[2\] We started reporting the number of accounts for which at least some data was provided in response to government requests in our January-June 2014 Transparency Report.  

Breakdown of U.S. government requests for data
----------------------------------------------

#### 2023: January-June

|     |     |
| --- | --- |
| Requests \[1\] | 386 |
| Accounts subject to requests | 2,053 |
| Requests for which LinkedIn provided some data | 83% |
| Subpoenas \[2\] | 76% |
| Search warrants \[3\] | 6%  |
| Court orders \[4\] | 17% |
| Other \[5\] | 1%  |
| National security letters received \[6\] | 0-499 |
| National security letters (accounts subject to request) | 0-499 |
| FISA requests \[6\] | 0-499 |

#### 2022: July-December

|     |     |
| --- | --- |
| Requests \[1\] | 391 |
| Accounts subject to requests | 2,636 |
| Requests for which LinkedIn provided some data | 80% |
| Subpoenas \[2\] | 77% |
| Search warrants \[3\] | 7%  |
| Court orders \[4\] | 15% |
| Other \[5\] | 1%  |
| National security letters received \[6\] | 0-499 |
| National security letters (accounts subject to request) | 0-499 |
| FISA requests \[6\] | 0-499 |

#### 2021: July-December

|     |     |
| --- | --- |
| Requests \[1\] | 302 |
| Accounts subject to requests | 1,190 |
| Requests for which LinkedIn provided some data | 83% |
| Subpoenas \[2\] | 73% |
| Search warrants \[3\] | 9%  |
| Court orders \[4\] | 18% |
| Other \[5\] | 1%  |
| National security letters received \[6\] | 0-499 |
| National security letters (accounts subject to request) | 0-499 |
| FISA requests \[6\] | 0-499 |

#### 2021: January-June

|     |     |
| --- | --- |
| Requests \[1\] | 362 |
| Accounts subject to requests | 2,349 |
| Requests for which LinkedIn provided some data | 83% |
| Subpoenas \[2\] | 70% |
| Search warrants \[3\] | 9%  |
| Court orders \[4\] | 21% |
| Other \[5\] | 1%  |
| National security letters received \[6\] | 0-499 |
| National security letters (accounts subject to request) | 0-499 |
| FISA requests \[6\] | 0-499 |

#### 2020: July-December

|     |     |
| --- | --- |
| Requests \[1\] | 364 |
| Accounts subject to requests | 1,420 |
| Requests for which LinkedIn provided some data | 76% |
| Subpoenas \[2\] | 66% |
| Search warrants \[3\] | 11% |
| Court orders \[4\] | 22% |
| Other \[5\] | 1%  |
| National security letters received \[6\] | 0-499 |
| National security letters (accounts subject to request) | 0-499 |
| FISA requests \[6\] | 0-499 |

#### 2020: January-June

|     |     |
| --- | --- |
| Requests \[1\] | 377 |
| Accounts subject to requests | 2,459 |
| Requests for which LinkedIn provided some data | 78% |
| Subpoenas \[2\] | 63% |
| Search warrants \[3\] | 11% |
| Court orders \[4\] | 25% |
| Other \[5\] | 1%  |
| National security letters received \[6\] | 0-499 |
| National security letters (accounts subject to request) | 0-499 |
| FISA requests \[6\] | 0-499 |

#### 2019: July-December

|     |     |
| --- | --- |
| Requests \[1\] | 349 |
| Accounts subject to requests | 1,488 |
| Requests for which LinkedIn provided some data | 82% |
| Subpoenas \[2\] | 68% |
| Search warrants \[3\] | 12% |
| Court orders \[4\] | 20% |
| Other \[5\] | 0%  |
| National security letters received \[6\] | 0-499 |
| National security letters (accounts subject to request) | 0-499 |
| FISA requests \[6\] |     |

#### 2019: January-June

|     |     |
| --- | --- |
| Requests \[1\] | 314 |
| Accounts subject to requests | 1,170 |
| Requests for which LinkedIn provided some data | 82% |
| Subpoenas \[2\] | 72% |
| Search warrants \[3\] | 10% |
| Court orders \[4\] | 17% |
| Other \[5\] | 1%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 250-499 |

#### 2018: July-December

|     |     |
| --- | --- |
| Requests \[1\] | 203 |
| Accounts subject to requests | 1,069 |
| Requests for which LinkedIn provided some data | 52% |
| Subpoenas \[2\] | 71% |
| Search warrants \[3\] | 11% |
| Court orders \[4\] | 16% |
| Other \[5\] | 2%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 0-249 |

#### 2018: January-June

|     |     |
| --- | --- |
| Requests \[1\] | 196 |
| Accounts subject to requests | 1,077 |
| Requests for which LinkedIn provided some data | 37% |
| Subpoenas \[2\] | 70% |
| Search warrants \[3\] | 9%  |
| Court orders \[4\] | 19% |
| Other \[5\] | 3%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 0-249 |

#### 2017: July-December

|     |     |
| --- | --- |
| Requests \[1\] | 191 |
| Accounts subject to requests | 602 |
| Requests for which LinkedIn provided some data | 69% |
| Subpoenas \[2\] | 62% |
| Search warrants \[3\] | 8%  |
| Court orders \[4\] | 26% |
| Other \[5\] | 1%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 0-249 |

#### 2017: January-June

|     |     |
| --- | --- |
| Requests \[1\] | 187 |
| Accounts subject to requests | 594 |
| Requests for which LinkedIn provided some data | 71% |
| Subpoenas \[2\] | 63% |
| Search warrants \[3\] | 9%  |
| Court orders \[4\] | 26% |
| Other \[5\] | 1%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 0-249 |

#### 2016: July-December

|     |     |
| --- | --- |
| Requests \[1\] | 135 |
| Accounts subject to requests | 345 |
| Requests for which LinkedIn provided some data | 74% |
| Subpoenas \[2\] | 71% |
| Search warrants \[3\] | 9%  |
| Court orders \[4\] | 18% |
| Other \[5\] | 1%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 0-249 |

#### 2016: January-June

|     |     |
| --- | --- |
| Requests \[1\] | 128 |
| Accounts subject to requests | 291 |
| Requests for which LinkedIn provided some data | 68% |
| Subpoenas \[2\] | 66% |
| Search warrants \[3\] | 14% |
| Court orders \[4\] | 17% |
| Other \[5\] | 2%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 0-249 |

#### 2015: July-December

|     |     |
| --- | --- |
| Requests \[1\] | 126 |
| Accounts subject to requests | 226 |
| Requests for which LinkedIn provided some data | 72% |
| Subpoenas \[2\] | 72% |
| Search warrants \[3\] | 11% |
| Court orders \[4\] | 16% |
| Other \[5\] | 2%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 0-249 |

#### 2015: January-June

|     |     |
| --- | --- |
| Requests \[1\] | 99  |
| Accounts subject to requests | 141 |
| Requests for which LinkedIn provided some data | 78% |
| Subpoenas \[2\] | 83% |
| Search warrants \[3\] | 10% |
| Court orders \[4\] | 7%  |
| Other \[5\] | 1%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 0-249 |

#### 2014: July-December

|     |     |
| --- | --- |
| Requests \[1\] | 84  |
| Accounts subject to requests | 202 |
| Requests for which LinkedIn provided some data | 70% |
| Subpoenas \[2\] | 86% |
| Search warrants \[3\] | 12% |
| Court orders \[4\] | 2%  |
| Other \[5\] | 0%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 0-249 |

#### 2014: January-June

|     |     |
| --- | --- |
| Requests \[1\] | 85  |
| Accounts subject to requests | 1,069 |
| Requests for which LinkedIn provided some data | 84% |
| Subpoenas \[2\] | 82% |
| Search warrants \[3\] | 13% |
| Court orders \[4\] | 4%  |
| Other \[5\] | 1%  |
| National security requests received \[6\] | 0-249 |
| National security requests (accounts subject to request) | 0-249 |

\[1\] U.S. Government Requests for Member Data include all requests received by LinkedIn from the U.S. government except for national security-related requests, such as National Security Letters and requests under the U.S. Foreign Intelligence Surveillance Act (FISA), if any.  
  

\[2\] Subpoenas may be issued for information that is reasonably relevant to the general subject matter of a pending investigation. They are typically pre-signed by a court clerk and are issued by prosecutors without the involvement of a judge.  
  

\[3\] Search warrants require the government to demonstrate "probable cause" and are generally issued by a judge. The standard applicable to a search warrant is higher than that applicable to a subpoena.  

\[4\] Court orders vary depending on the circumstances and the issuing court and jurisdiction. Many of the court orders LinkedIn receives are issued pursuant to 18 U.S.C. § 2703(d), a provision of the Electronic Communications Privacy Act (ECPA). To obtain such an order, the government must demonstrate specific and articulable facts showing that there are reasonable grounds to believe that the information sought is relevant and material to an ongoing investigation. This standard is higher than that applicable to subpoenas but lower than that applicable to search warrants.

\[5\] The "Other" category includes requests that do not fall within any of the above categories. Examples include emergency requests. As indicated in footnote 1, the category does not include national security-related requests.  

  
\[6\] In our Government Requests Reports prior to July 2019, we reported on National Security Requests, which could include National Security Letters and FISA requests. In the effort to provide more transparency to our members, we now report NLSs and FISA requests separately. Because we are required by law to delay the release of FISA data by 6 months, reported FISA request metrics correspond to the previous reporting period (for example, data for the July-December 2019 period is reported in our January-June 2020 report).

Government requests for content removal
---------------------------------------

### By country and action taken

#### 2023: January-June

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| Australia | 2   | 1   | 50% |
| Bahrain | 1   | 0   | 0%  |
| Bangladesh | 1   | 1   | 100% |
| Bolivia | 1   | 1   | 100% |
| Brazil | 4   | 4   | 100% |
| China | 5   | 4   | 80% |
| Ghana | 1   | 1   | 100% |
| India | 13  | 12  | 92% |
| Indonesia | 8   | 8   | 100% |
| Qatar | 1   | 0   | 0%  |
| Singapore | 1   | 1   | 100% |
| Turkey | 5   | 5   | 100% |
| United Kingdom | 1   | 1   | 100% |
| United States | 5   | 5   | 100% |
| **Totals** | **49** | **44** | **90%** |

### By country and action taken

#### 2022: July-December

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| Australia | 2   | 2   | 100% |
| Canada | 1   | 1   | 100% |
| India | 6   | 4   | 67% |
| Indonesia | 4   | 4   | 100% |
| Turkey | 10  | 9   | 90% |
| **Totals** | **23** | **20** | **87%** |

### By country and action taken

#### 2022: January-June

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| Australia | 1   | 1   | 100% |
| Brazil | 1   | 1   | 100% |
| France | 1   | 1   | 100% |
| Germany | 1   | 1   | 100% |
| Ghana | 1   | 0   | 0%  |
| India | 12  | 10  | 83% |
| Monaco | 1   | 0   | 0%  |
| Turkey | 8   | 8   | 100% |
| UK  | 1   | 1   | 100% |
| **Totals** | **27** | **23** | **85%** |

### By country and action taken

#### 2021: July-December

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| China | 12  | 12  | 100% |
| India | 5   | 4   | 80% |
| Turkey | 10  | 10  | 100% |
| **Totals** | **27** | **26** | **96%** |

### By country and action taken

#### 2021: January-June

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| China | 31  | 30  | 97% |
| India | 7   | 4   | 57% |
| Turkey | 24  | 23  | 96% |
| **Totals** | **62** | **57** | **92%** |

### By country and action taken

#### 2020: July-December

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| Turkey | 5   | 4   | 80% |
| China | 24  | 22  | 92% |
| **Totals** | **29** | **26** | **89%** |

### By country and action taken

#### 2020: January-June

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| US  | 1   | 1   | 100% |
| Turkey | 2   | 2   | 100% |
| China | 18  | 16  | 89% |
| **Totals** | **21** | **19** | **90%** |

### By country and action taken

#### 2019: July-December

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| Brazil | 1   | 1   | 100% |
| China | 14  | 11  | 79% |
| Greece | 1   | 1   | 100% |
| India | 1   | 1   | 100% |
| Poland | 1   | 1   | 100% |
| Russia | 2   | 2   | 100% |
| Switzerland | 1   | 1   | 100% |
| Turkey | 2   | 2   | 100% |
| **Totals** | **23** | **20** | **87%** |

### By country and action taken

#### 2019: January-June

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| Canada | 1   | 1   | 100% |
| China | 3   | 3   | 100% |
| Germany | 1   | 1   | 100% |
| Saudi Arabia | 1   | 1   | 100% |
| Turkey | 2   | 2   | 100% |
| **Totals** | **8** | **8** | **100%** |

### By country and action taken

#### 2018: July-December

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| Turkey | 1   | 1   | 100% |
| China | 4   | 2   | 50% |
| India | 1   | 1   | 100% |
| **Totals** | **6** | **4** | **67%** |

### By country and action taken

#### 2018: January-June

|     |     |     |     |
| --- | --- | --- | --- |
|     | Requests | Action taken | Percentage action taken |
| Canada | 1   | 1   | 100% |
| China | 9   | 9   | 100% |
| India | 2   | 1   | 50% |
| Indonesia | 1   | 1   | 100% |
| United Kingdom | 1   | 1   | 100% |
| United States | 1   | 1   | 100% |
| **Totals** | **15** | **14** | **93.33%** |

• We started reporting on content-related requests in our January 1-June 30, 2018 Transparency Report.

  
• Government Requests for Content Removal are requests received by LinkedIn from governments seeking the removal of content, including reporting violations of our Terms of Service or violations of local law.