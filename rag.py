import pandas as pd
import sys
import os
from infer import rag

path_questions = sys.argv[1]
path_output = sys.argv[2]

df = pd.read_csv(path_questions, sep=";")

for k, row in df.iterrows():
    id_ = int(row["id"])
    answer, source = rag(row["question"])
    if id_ in range(0,10):
        id_ = "00"+str(id_)
    elif id_ in range(10, 100):
        id_ = "0"+str(id_)
    else:
        id_ = str(id_)
    os.makedirs(os.path.join(path_output, "answers"), exist_ok=True)
    os.makedirs(os.path.join(path_output, "sources"), exist_ok=True)
    with open(os.path.join(path_output, "answers", id_+ ".txt"), "w") as f:
        f.write(answer)
    with open(os.path.join(path_output, "sources", id_+ ".txt"), "w") as f:
        f.write(source)
