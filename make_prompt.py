from bge_embeddings import get_similar_chunks
MAX_TOKENS = 4096

def make_prompt_rag(
    query="", limit=4, system_instruct=""
):
    prompt = []

    if not system_instruct:
        system_instruct = (
            "Use the information below to answer the final question. If you don't know the answer, simply say you don't know, don't try to invent an answer. Please answer in the language of the question"
        )
    prompt.append(system_instruct)

    # Rag
    limit = 3 if limit is None else limit
    chunks = get_similar_chunks(query, top_n=limit)
    chunks = "\n\n".join(chunks)
    prompt.append(f"{chunks}")

    prompt.append(f"Question : {query}")
    prompt = "\n\n".join(prompt)

    if limit > 1 and len(prompt.split()) * 1.25 > 3 / 4 * MAX_TOKENS:
        return make_prompt_rag(query, limit=limit - 1)

    return prompt

def make_prompt_get_main_source(query="", limit=4):
    system_instruct = "Use the information below to find the URL answering the question. Your answer should only be the URL. Don't answer anything more than the URL."
    return make_prompt_rag(query, limit, system_instruct)


if __name__ == "__main__":
    make_prompt_rag("What is the url of Linkedin transparency page?")