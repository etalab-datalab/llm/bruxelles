import os

import torch

# Root directory:
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))


# CORS
BACKEND_CORS_ORIGINS = [
    "http://localhost:4173",
    "http://localhost:8080",
]


# Urls
# @obsolete: use HOST instead of URL, this is confusing
API_VLLM_URL = "http://127.0.0.1:8000"  # default
ELASTICSEARCH_URL = "http://127.0.0.1:9202"
ELASTICSEARCH_CREDS = ("elastic", "changeme")
QDRANT_URL = "http://127.0.0.1:6333"

# Search Engines
ELASTICSEARCH_IX_VER = "v1"
QDRANT_IX_VER = "v1"

#EMBEDDING_MODEL = "intfloat/multilingual-e5-large"
EMBEDDING_MODEL = "BAAI/bge-m3"


CORPUS_PATH = os.path.join("_data", "cgu")
HTML_OUTPUT_PATH = os.path.join("_data", "html")
CHUNKS_PATH = os.path.join("_data", "chunks_all.csv")

EMBEDDING_BOOTSTRAP_PATH = os.path.join("_data", "embeddings", EMBEDDING_MODEL.split("/")[-1])
EMBEDDINGS_PATH = os.path.join(EMBEDDING_BOOTSTRAP_PATH,'embeddings_e5_chunks_all.npy')
BGE_PATH = os.path.join("BAAI/bge-m3")

# For local testing, use:
# --
# API_ROUTE_VER = "/"
# API_URL = "http://localhost:8000"


def collate_ix_name(name, version):
    if version:
        return "-".join([name, version])
    return name

if torch.cuda.is_available():
    WITH_GPU = True
    DEVICE_MAP = "cuda:0"
else:
    WITH_GPU = False
    DEVICE_MAP = None
