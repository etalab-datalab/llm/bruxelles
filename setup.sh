#!/bin/sh

python app_vllm.py --model ${VLLM_MODEL} --tensor-parallel-size 1 --gpu-memory-utilization 0.4 --host localhost --port 8000
