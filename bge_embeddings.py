import os
from FlagEmbedding import BGEM3FlagModel
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
from config import CHUNKS_PATH, BGE_PATH, EMBEDDINGS_PATH


def make_embeddings():

    model = BGEM3FlagModel(BGE_PATH, use_fp16=True) 
    # Setting use_fp16 to True speeds up computation with a slight performance degradation
    #sp_simple = pd.read_csv("dataset_rag.csv")
    #sentences_1 = sp_simple["text"].tolist()
    #links_1 = sp_simple["url"]

    texts = pd.read_csv(CHUNKS_PATH, sep="~")["text"].values
    embeddings_1 = model.encode(texts, 
                                batch_size=12, 
                                max_length=8192, # If you don't need such a long length, you can set a smaller value to speed up the encoding process.
                                )['dense_vecs']
    np.save(os.path.join(EMBEDDINGS_PATH,'embeddings_e5_chunks.npy'), embeddings_1)

def get_similar_chunks(query: str, top_n:int=3) -> list[str]:
    """
    

    Args:
        query (str): _description_

    Returns:
        list[str]: the most similar chunks
    """
    df = pd.read_csv(CHUNKS_PATH, sep="~")
    texts = df["text"].values
    urls  = df["URL"].values
    embeddings = np.load(os.path.join(EMBEDDINGS_PATH))

    model = BGEM3FlagModel(BGE_PATH, use_fp16=True) 
    query_embedding = model.encode(
        [query], 
        batch_size=12, 
        max_length=8192, # If you don't need such a long length, you can set a smaller value to speed up the encoding process.
    )['dense_vecs']


    # Reshape the query embedding to fit the cosine_similarity function requirements
    query_embedding_reshaped = query_embedding.reshape(1, -1)

    # Compute cosine similarities
    # TODO: yes, it could be directly performed on an axis. improve it later
    similarities = cosine_similarity(query_embedding_reshaped, embeddings)
    # Find the index of the closest document (highest similarity)
    sorted_indices = np.argsort(similarities)[0]
#    Get the indices of the top n nearest neighbors
    top_n_indices = sorted_indices[-top_n:]

    # Closest document's embedding
    answers = [
        texts[ind] + "\n " + "URL: "+ urls[ind] for ind in top_n_indices
    ]
    return answers



if __name__ == "__main__":
   # make_embeddings()
    sentence_query = "Où puis-je trouver le centre de transparence de Linkedin ? Veuillez indiquer une seule URL."
    get_similar_chunks(sentence_query, top_n=5)

