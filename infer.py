import json
import requests
from typing import Iterable, Tuple
from config import API_VLLM_URL
from make_prompt import make_prompt_rag, make_prompt_get_main_source

def get_response(response: requests.Response) -> str:
    data = json.loads(response.content)
    output = data["text"]
    # Beams ignored
    return output[0]


def get_streaming_response(response: requests.Response) -> Iterable[str]:
    prev_len = 0
    chunks = response.iter_lines(chunk_size=8192, delimiter=b"\0")
    for chunk in chunks:
        if not chunk:
            continue

        data = json.loads(chunk.decode("utf-8"))
        # Beams ignored
        output = data["text"][0]
        yield output[prev_len:]
        prev_len = len(output)


class ApiVllmClient:
    def __init__(self, url=None):
        if not url:
            url = API_VLLM_URL

        self.url = url

    # TODO: turn into async
    def generate(
        self, prompt, max_tokens=512, temperature=20, top_p=1, streaming=True
    ) -> str | Iterable[str]:
        url = f"{self.url}/generate"
        data = {
            "prompt": prompt,
            "max_tokens": max_tokens,
            "temperature": temperature
            / 100,  # it thinks its better to keep [0,2] value to stay compatible with opanai api. The client can do this operation, if it implement a slider...
            "top_p": top_p,  # not intended to final user but for dev and research.
            "stream": streaming,
        }
        response = requests.post(url, json=data, stream=streaming)

        if streaming:
            return get_streaming_response(response)
        else:
            return get_response(response)

def infer(prompt: str) -> str:
    """
    Return the answer of the LLM

    Args:
        prompt (str): _description_

    Returns:
        str: _description_
    """
    api_vllm_client = ApiVllmClient(url=API_VLLM_URL)
    llm_response = api_vllm_client.generate(prompt, temperature=20, streaming=False)
    return llm_response


def rag(query: str) -> Tuple[str, str]:
    """
    Return both the answer and the url (also a text) of the source

    Args:
        query (str): the question

    Returns:
        Tuple[str, str]: _description_
    """

    prompt_rag = make_prompt_rag(query, limit=3)
    prompt_source = make_prompt_get_main_source(query, limit=3)
    return infer(prompt_rag), infer(prompt_source)

if __name__ == "__main__":
    print(
        infer(
            """Use the information below to answer the final question. If you don't know the answer, simply say you don't know, don't try to invent an answer. Please answer in the language of the question

            TikTok\_Transparency
            Resource URL: https://www.tiktok.com/transparency/en/copd-eu/
            Code of Practice on Disinformation
            URL:  https://www.tiktok.com/transparency/en/dsa-transparency/

            Linkedin\_Ad-Library
            Ad Library
            LinkedIn’s Ad Library offers transparency in advertising by providing a searchable collection of ads.
            URL:  https://www.linkedin.com/ad-library/

            Question : What is the url of Linkedin transparency page?
            """
        )
    )