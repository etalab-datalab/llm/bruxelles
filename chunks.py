from config import CORPUS_PATH, API_CHUNKS_PATH, TERMS_CHUNKS_PATH
import pypandoc
from bs4 import BeautifulSoup
import pathlib
from typing import Tuple, List
import pandas as pd
import re

CHUNK_MAX_LENGTH = 4000


def get_files_metadata(corpus_path: pathlib.Path) -> List[Tuple[pathlib.Path, str, str]]:
    """
    Read all files names and tag if they are api or terms, and get the parent folder name  

    Args:
        corpus_path (pathlib.Path): _description_

    Returns:
        list[[pathlib.Path, str, str]: for each file, give the path, the nature ("api" or "terms") and the title 
    """
    metadata = list()
    for file in corpus_path.rglob("*.md"):
        try:
            title=str(file).split("/")[-2] # get the name of the folder
        except:
            title = ""
        if str(file).endswith("README.md"):
            continue
        elif "API" in str(file):
            metadata.append(
                [file, "api", title]
            )
        else:
            metadata.append(
                [file, "terms", title]
            )    
    return metadata

def remove_text_after_heading(html_str):
        match = re.search(r'<h[2-6]>', html_str)
        if match:
            return html_str[:match.start()]
        return html_str

def extract_section_content(start_heading):
    """
    extract text from a section until the next h1 or h2

    Args:
        start_heading (_type_): _description_

    Returns:
        content: text
    """
    content = []
    # stop at the next h1 or h2 tag
    for sibling in start_heading.next_siblings:
        if sibling.name and (sibling.name.startswith('h1') or sibling.name.startswith('h2')):
            break
        if sibling.name:  # Checking if it's a tag and not a NavigableString
            sibling_html_str = str(sibling)
            cleaned_html_str = remove_text_after_heading(sibling_html_str)
            cleaned_soup = BeautifulSoup(cleaned_html_str, 'html.parser')
            content.append(' '.join(cleaned_soup.stripped_strings))
    return ' '.join(content)


def get_chunks(soup: BeautifulSoup, main_title: str="") -> Tuple[list[str], str]:
    """
    Chunk html content according to h1 and h2 titles

    Args:
        soup (BeautifulSoup): _description_

    Returns:
        Tuple[list[str], str]: a list of all chunks + the URL of the source
    """
    # get first title
    #  assuming an URL is always in the first h1 and introduced with 'Resource URL:'
    if soup.find("h1"):
        resource_url = soup.find("h1").text.split("Resource URL:")[-1]
        tags_title = soup.find_all(["h1","h2"])[1:]
    # Then exclu

    else:
        resource_url = ""
        tags_title = soup.find_all(["h1","h2"])


    texts = list()

    for i in range(len(tags_title) - 1):
        beginning = tags_title[i]
        ending = tags_title[i + 1]

        # Get sections
        #inner_text = ''.join([str(sibling.text) for sibling in beginning.find_next_siblings() if sibling != ending])
        inner_text = extract_section_content(beginning)
        texts.append(main_title + "\n" + beginning.text + "\n" + inner_text)

    # get last title
    #if tags_title:
    #    last_tag = tags_title[-1]
    #    last_text = ''.join([str(sibling.text) for sibling in last_tag.find_next_siblings()])
    #    texts.append(main_title + "\n" + last_tag.text + "\n" + last_text)
    return texts, resource_url

records_terms = list()
records_api = list()

for metadata in get_files_metadata(corpus_path=pathlib.Path(CORPUS_PATH)):
    file, nature, title = metadata
    #data = pypandoc.convert_file(file, to="html5", outputfile=os.path.join(HTML_OUTPUT_PATH, str(file).replace("/", "_").split(".")[0] + ".html"))
    data = pypandoc.convert_file(file, to="html5")
    soup = BeautifulSoup(data, "html.parser")
    texts, resource_url = get_chunks(soup=soup, main_title=title)
    if nature == "api":
        records_api.extend(
            [
                {"text": text[:CHUNK_MAX_LENGTH], "URL": resource_url}
                for text in texts
            ]
        )
    else:
        records_terms.extend(
            [
                {"text": text[:CHUNK_MAX_LENGTH], "URL": resource_url}
                for text in texts
            ]
        )
    
pd.DataFrame(records_api).to_csv(API_CHUNKS_PATH, sep="~")
pd.DataFrame(records_terms).to_csv(TERMS_CHUNKS_PATH, sep="~")
